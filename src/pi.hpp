#ifndef _PI_HPP_
#define _PI_HPP_
#include <stddef.h>
#include <string>

void question4(size_t nb_replications = 10, size_t nb_draws = 1'000'000'000, const std::string &file_name = "mt3_");

void question5(const std::string &file_name = "mt3_", size_t nb_draws = 1'000'000'000);

void question6a_future(const std::string &file_name = "mt3_", size_t nb_draws = 1'000'000'000);

void question6a_threads(const std::string &file_name = "mt3_", size_t nb_draws = 1'000'000'000);

#endif
