#include "status.hpp"
#include "utils.hpp"

#include <CLHEP/Random/MTwistEngine.h>
#include <assert.h>
#include <cstddef>

// conf
constexpr size_t NB_FILE = 5;
constexpr size_t SEPARE = 10;

// question 2
void question2()
{
    CLHEP::MTwistEngine mt;
    double saved[NB_FILE][SEPARE];
    // save statut and tirages
    for (size_t i = 0; i < NB_FILE; ++i)
    {
        mt.saveStatus(cat("mt2_", i).c_str());
        for (size_t j = 0; j < SEPARE; ++j) { saved[i][j] = mt.flat(); }
    }
    // check tirages
    for (size_t i = 0; i < NB_FILE; ++i)
    {
        mt.restoreStatus(cat("mt2_", i).c_str());
        for (size_t j = 0; j < SEPARE; ++j)
        {
            // can have an issue here!!
            assert(saved[i][j] == mt.flat());
        }
    }
}

// question 3
void question3(size_t status_file, size_t separaration_nb, const std::string &file)
{
    CLHEP::MTwistEngine mt;
    // save statut and tirages
    for (size_t i = 0; i < status_file; ++i) {
        mt.saveStatus(cat(file, i).c_str());
        for (size_t j = 0; j < separaration_nb; ++j) { mt.flat(); }
    }
}
