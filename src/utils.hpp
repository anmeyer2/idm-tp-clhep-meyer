#ifndef _UTILS_HPP_
#define _UTILS_HPP_
#include <chrono>
#include <bits/chrono.h>
#include <mutex>
#include <sstream>

// NOTE : Inspiré par et discuté avec Rémi CHASSAGNOL

// globals
extern std::mutex mutex;

// macros
#define timer_start() auto _start = std::chrono::high_resolution_clock::now();
#define timer_end() auto _end = std::chrono::high_resolution_clock::now();
#define timer_count()                                                          \
    std::chrono::duration_cast<std::chrono::microseconds>(_end - _start)       \
            .count() /                                                         \
        1000.0

// functions
inline std::string cat(const std::string &str, int i)
{
    std::ostringstream oss;
    oss << str << i;
    return oss.str();
}

#endif
