#include "pi.hpp"
#include "utils.hpp"

#include <CLHEP/Random/MTwistEngine.h>
// question 6a version thread
#include <thread>
// question 6a version future
#include <future>

// compute pi with monte carlo
double compute_pi(CLHEP::HepRandomEngine &generator, int nb_draw) {
    long nb_valid_points = 0;

    for (int i = 0; i < nb_draw; ++i) {
        double x = generator.flat();
        double y = generator.flat();

        if (x * x + y * y < 1) { ++nb_valid_points; }
    }
    return 4 * (double)nb_valid_points / nb_draw;
}

// question 4
void question4(size_t nb_replications, size_t nb_draws, const std::string &file_name) {
    CLHEP::MTwistEngine mt;
    double sumPI = 0;

    timer_start();
    for (size_t i = 0; i < nb_replications; ++i) {
        mt.restoreStatus(cat(file_name, i).c_str());
        sumPI += compute_pi(mt, nb_draws);
    }
    timer_end();

    std::cout << std::setprecision(10) << "pi: " << sumPI / nb_replications << "; time: " << timer_count() << std::endl;
}

// question 5
void question5(const std::string &file_name, size_t nb_draws) {
    CLHEP::MTwistEngine mt;
    double pi = 0;

    timer_start();
    mt.restoreStatus(file_name.c_str());
    pi = compute_pi(mt, nb_draws);
    timer_end();

    /* mutex lock on std::cout */
    {
        std::unique_lock<std::mutex> lock(mutex);
        std::cout << "pi: " << pi << " (with " << file_name << "); time: " << timer_count() << "ms" << std::endl;
    }
}

// question 6a
constexpr size_t NB_REPLI = 10;

// version future
void question6a_future(const std::string &file_name, size_t nb_draws) {
    CLHEP::MTwistEngine mt;
    size_t i;

    timer_start();
    /* threads */
    {
        std::future<void> pis[NB_REPLI];
        for (i = 0; i < NB_REPLI; ++i) { pis[i] = std::async(std::launch::async, question5, cat(file_name, i), nb_draws); }
    }
    timer_end();
    std::cout << "total time: " << timer_count() / 1000.0 << "s" << std::endl;
}

// version threads
void question6a_threads(const std::string &file_name, size_t nb_draws) {
    constexpr size_t NB_REPLI = 10;
    std::thread thread[NB_REPLI];
    CLHEP::MTwistEngine mt;
    size_t i;

    timer_start();
    for (i = 0; i < NB_REPLI; ++i) { thread[i] = std::thread(question5, cat(file_name, i), nb_draws); }

    for (i = 0; i < NB_REPLI; ++i) { thread[i].join(); }
    timer_end();

    std::cout << "total time: " << timer_count() / 1000.0 << "s" << std::endl;
}
