#include "status.hpp"
#include "pi.hpp"
#include "gattaca.hpp"
#include "utils.hpp"

#include <CLHEP/Random/MTwistEngine.h>
#include <iomanip>
#include <iostream>

// globales
std::mutex mutex;

// command
enum ACTIONS {
    TEST,
    GEN_STATUS,
    PI_, PI_PARALLEL_THREADS, PI_PARALLEL_FUTURE,
    /*GATTACA,*/ GATTACA_GENERATE,
    PI_INPUT_FILE,
};

ACTIONS parse_command_line(int argc, char **argv) {
    if (argc >= 2) {
        if (std::string(argv[1]) == "test")                                 { return TEST; }
        else if (std::string(argv[1]) == "pi") {
            if (argc >= 3 && std::string(argv[2]) == "parallel-threads")    { return PI_PARALLEL_THREADS; }
            else if(argc >= 3 && std::string(argv[2]) == "parallel-future") { return PI_PARALLEL_FUTURE; }
            else                                                            { return PI_; } }
        else if (std::string(argv[1]) == "gattaca") {
            if (argc >= 3 && std::string(argv[2]) == "generate")            { return GATTACA_GENERATE; }
            else                                                            { /*return GATTACA;*/ } }
        else                                                                { return PI_INPUT_FILE; }
    }
    return GEN_STATUS;
}

// main
int main(int argc, char **argv) {
    std::cout << std::setprecision(10);
    switch (parse_command_line(argc, argv)) {
        case TEST:                  question2();                                        break;
        case GEN_STATUS:            question3();                                        break;
        case PI_:                   question4();                                        break;
        case PI_PARALLEL_THREADS:   question6a_threads();                               break;
        case PI_PARALLEL_FUTURE:    question6a_future();                                break;
        case GATTACA_GENERATE:      question3(40, 400'000'000, "gattaca/gattaca_");     break;
        //case GATTACA:             question6b(40, 400'000'000, "gattaca/gattaca_");    break;
        case PI_INPUT_FILE:         question5(argv[1]);                                 break;
    }
    return 0;
}
