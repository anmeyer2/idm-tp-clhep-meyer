# idm-tp-clhep-meyer

## Installation

```sh
git submodule init
git submodule update
cd lib
bash build.sh
```

## Compilation

```sh
mkdir build
cd build
cmake ..
make
```

## Utilisation

```sh
# question 3, mandatory
./idm-tp-clhep-meyer

# question 2
./idm-tp-clhep-meyer test

# question 4
./idm-tp-clhep-meyer pi

# question 5
bash question5_pi.sh

# question 6a threads
./idm-tp-clhep-meyer pi parallel-threads

# question 6a future
./idm-tp-clhep-meyer pi parallel-future

# question 6b
./idm-tp-clhep-meyer gattaca generate
# ./idm-tp-clhep-meyer gattaca
```
